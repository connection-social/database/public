CREATE SEQUENCE IF NOT EXISTS sequence_user_account_id;

CREATE TABLE IF NOT EXISTS user_account
(
    id                    INT       NOT NULL DEFAULT NEXTVAL('sequence_user_account_id'),
    email                 VARCHAR   NOT NULL,
    first_name            VARCHAR   NOT NULL,
    last_name             VARCHAR   NOT NULL,
    description           VARCHAR   NOT NULL,
    hobby                 VARCHAR   NOT NULL,
    public_profile_image  VARCHAR   NOT NULL,
    private_profile_image VARCHAR            DEFAULT NULL,
    phone_number          VARCHAR            DEFAULT NULL,
    range_age             VARCHAR            DEFAULT NULL,
    birthday              DATE               DEFAULT NULL,
    last_connection       TIMESTAMP NOT NULL,
    confirmed_at          TIMESTAMP          DEFAULT NULL,
    position              VARCHAR            DEFAULT NULL,
    password              VARCHAR   NOT NULL,
    role                  VARCHAR   NOT NULL,
    connection_token      VARCHAR   NOT NULL,
    token                 VARCHAR            DEFAULT NULL,
    is_bank_image_public  BOOLEAN   NOT NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS user_account_id ON user_account (id);
CREATE UNIQUE INDEX IF NOT EXISTS user_account_connection_token ON user_account (connection_token);
CREATE UNIQUE INDEX IF NOT EXISTS user_account_email ON user_account (email);
CREATE INDEX IF NOT EXISTS user_account_age ON user_account (birthday);
CREATE INDEX IF NOT EXISTS user_account_hobby ON user_account (hobby);
CREATE INDEX IF NOT EXISTS user_account_description ON user_account (description);

ALTER TABLE user_account DROP COLUMN IF EXISTS position;
ALTER TABLE user_account ADD COLUMN IF NOT EXISTS longitude FLOAT DEFAULT NULL;
ALTER TABLE user_account ADD COLUMN IF NOT EXISTS latitude FLOAT DEFAULT NULL;
ALTER TABLE user_account ADD COLUMN IF NOT EXISTS distance INT DEFAULT NULL;
ALTER TABLE user_account ADD COLUMN IF NOT EXISTS authorization_get_position BOOLEAN DEFAULT NULL;

CREATE INDEX IF NOT EXISTS user_account_latitude ON user_account (latitude);
CREATE INDEX IF NOT EXISTS user_account_longitude ON user_account (longitude);
CREATE INDEX IF NOT EXISTS user_account_distance ON user_account (distance);
